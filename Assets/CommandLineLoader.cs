﻿using System;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

public class CommandLineLoader : Editor
{
    // Run with
    // ./Unity -projectPath ~/cantloaddemo -executeMethod CommandLineLoader.LoadTestScenes -logfile "~/cantloaddemo/logs.txt" -batchmode
    public static void LoadTestScenes()
    {
        try
        {
            // Set editor mode to Android
            EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Android, BuildTarget.Android);

            EditorSceneManager.OpenScene($"Assets/Scenes/SampleScene.unity");

            EditorApplication.isPlaying = true;
            Debug.Log($"Entered playmode");
        }
        catch (Exception e)
        {
            Debug.Log($"Had exception {e}");
        }
        Debug.Log($"Complete CommandLineLoader.LoadTestScenes");
    }
}