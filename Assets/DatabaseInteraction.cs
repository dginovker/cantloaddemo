﻿using System;
using Firebase;
using Firebase.Database;
using Firebase.Extensions;
using UnityEngine;

public class DatabaseInteraction : MonoBehaviour
{
    private FirebaseApp _app = null;
    private bool _firebaseCodeRan = false;
        
    // Start is called before the first frame update
    void Start()
    {
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
            var dependencyStatus = task.Result;
            if (dependencyStatus == DependencyStatus.Available)
            {
                // Create and hold a reference to your FirebaseApp,
                // where app is a Firebase.FirebaseApp property of your application class.
                _app = FirebaseApp.DefaultInstance;
                Debug.Log($"Firebase initialized");

                // Set a flag here to indicate whether Firebase is ready to use by your app.
            } else {
                Debug.LogError(String.Format(
                    "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                // Firebase Unity SDK is not safe to use here.
            }
        }); 
    }

    // Update is called once per frame
    void Update()
    {
        if (_app != null && !_firebaseCodeRan)
        {
            _firebaseCodeRan = true;
            FirebaseDatabase.GetInstance("https://completelyreadabledatabase-default-rtdb.firebaseio.com/").GetReference("/")
                .GetValueAsync().ContinueWithOnMainThread(task => {
                    if (task.IsFaulted)
                    {
                        Debug.LogError(task.Exception);
                    }
                    else if (task.IsCompleted)
                    {
                        Debug.Log($"{task.Result.GetRawJsonValue()}");
                    }
                });
        }
    }
}